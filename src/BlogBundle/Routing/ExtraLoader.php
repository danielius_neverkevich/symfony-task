<?php

namespace BlogBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use BlogBundle\Entity\Route as DBRoute;

class ExtraLoader extends Loader
{
    private $loaded = false;
    private $em;

    function __construct($em)
    {
        $this->em = $em;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        $DBRoutes = $this->em->getRepository(DBRoute::class)->findAll();

        foreach ($DBRoutes as $newRoute) {

            $path = $newRoute->getPath();
            $defaults = array(
                '_controller' => $newRoute->getController(),
            );
            $requirements = array();

            $route = new Route($path, $defaults, $requirements);
            $route->setOption('utf8', true);
            $routes->add($newRoute->getName(), $route);

            foreach ($newRoute->getPathTranslations() as $item) {

                $subRoute = new Route($item->getPath(), $defaults, $requirements);
                $subRoute->setOption('utf8', true);
                $routes->add($newRoute->getName().'_'.$item->getId(), $subRoute);
            }
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'extra' === $type;
    }
}
