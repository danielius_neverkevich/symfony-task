<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Route
 *
 * @ORM\Table(name="route")
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\RouteRepository")
 */
class Route
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=255)
     */
    private $controller;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="parameters", type="string", length=255)
     */
    private $parameters;

    /**
     * @ORM\OneToMany(targetEntity="BlogBundle\Entity\RouteTranslation", mappedBy="route")
     */

    private $pathTranslations;

    function __construct()
    {
        $this->pathTranslations = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Route
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set controller
     *
     * @param string $controller
     *
     * @return Route
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Route
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parameters
     *
     * @param string $parameters
     *
     * @return Route
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Get parameters
     *
     * @return string
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set pathTranslations
     *
     * @param \BlogBundle\Entity\RouteTranslation $pathTranslations
     *
     * @return Route
     */
    public function setPathTranslations(\BlogBundle\Entity\RouteTranslation $pathTranslations = null)
    {
        $this->pathTranslations = $pathTranslations;

        return $this;
    }

    /**
     * Get pathTranslations
     *
     * @return \BlogBundle\Entity\RouteTranslation
     */
    public function getPathTranslations()
    {
        return $this->pathTranslations;
    }

    /**
     * Add pathTranslation
     *
     * @param \BlogBundle\Entity\RouteTranslation $pathTranslation
     *
     * @return Route
     */
    public function addPathTranslation(\BlogBundle\Entity\RouteTranslation $pathTranslation)
    {
        $this->pathTranslations[] = $pathTranslation;

        return $this;
    }

    /**
     * Remove pathTranslation
     *
     * @param \BlogBundle\Entity\RouteTranslation $pathTranslation
     */
    public function removePathTranslation(\BlogBundle\Entity\RouteTranslation $pathTranslation)
    {
        $this->pathTranslations->removeElement($pathTranslation);
    }
}
