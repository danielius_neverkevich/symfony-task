<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * route_translation
 *
 * @ORM\Table(name="route_translation")
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\route_translationRepository")
 */
class RouteTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity="BlogBundle\Entity\Route",
     *      inversedBy="pathTranslations"
     * )
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return route_translation
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->route = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add route
     *
     * @param \BlogBundle\Entity\Route $route
     *
     * @return RouteTranslation
     */
    public function addRoute(\BlogBundle\Entity\Route $route)
    {
        $this->route[] = $route;

        return $this;
    }

    /**
     * Remove route
     *
     * @param \BlogBundle\Entity\Route $route
     */
    public function removeRoute(\BlogBundle\Entity\Route $route)
    {
        $this->route->removeElement($route);
    }

    /**
     * Get route
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set route
     *
     * @param \BlogBundle\Entity\Route $route
     *
     * @return RouteTranslation
     */
    public function setRoute(\BlogBundle\Entity\Route $route = null)
    {
        $this->route = $route;

        return $this;
    }
}
