<?php

namespace BlogBundle\Controller;

use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BlogBundle\Entity\Route;
use BlogBundle\Entity\RouteTranslation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Filesystem\Filesystem;



class MainController extends Controller
{
    public function indexAction()
    {
        $routeList = $this->getDoctrine()->getRepository(Route::class)->findById(array(3,4));
        return $this->render('BlogBundle:Blog:index.html.twig', array('list' => $routeList));
    }

    public function listAction()
    {
        return $this->render('BlogBundle:Blog:blog.html.twig');
    }

    public function postViewAction($slug = null)
    {
        return $this->render('BlogBundle:Blog:view.html.twig', array('slug' => $slug));
    }

    public function newAction(Request $request)
    {

        $trans = new RouteTranslation();

        $form = $this->createFormBuilder($trans)
            ->add('route',EntityType::class, array('class' => 'BlogBundle:Route','choice_label' => 'path'))
            ->add('path', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

             $em = $this->getDoctrine()->getManager();
             $em->persist($trans);
             $em->flush();

            $fs = new Filesystem();
            $fs->remove($this->container->getParameter('kernel.cache_dir'));

            return $this->redirectToRoute('index');
        }

        return $this->render('BlogBundle:Blog:new.html.twig', array('form' => $form->createView()));
    }

}
